# Talk slides by Castedo

Thanks to [Albert Krewinkel](https://github.com/tarleb) for sharing his
public open source pandoc based slides.

<a href="http://creativecommons.org/licenses/by/4.0/" rel="nofollow">
  <img alt="CC BY 4.0" src="https://img.shields.io/badge/License-CC%20BY%204.0-lightgrey.svg" style="max-width: 100%;">
</a>
CC-BY 4.0 license

