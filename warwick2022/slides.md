---
title: Microscale estimation of admixture timing with stochastic processes of gametic lineages
author: Castedo Ellerman
institute: castedo.com/about
---

# Outline

1. Applications & Admixture ... with demo!!!
2. Lineages
3. An Estimator and Stochastic Process

# Empirical Application

When did populations mix?

* Inuit & Europeans
  * Greenland [1]
* Africans & Europeans
  * Barbados
  * Cabo Verde

[1] Waples RK, Hauptmann AL, Seiding I, et al (2021) The genetic history
of Greenlandic-European contact. Current biology: CB 31:2214–2219.e4.
https://doi.org/10.1016/j.cub.2021.02.041

<!--
# What do we mean by admixture?

* at a time horizon, all ancestors categorized into source populations
* individuals are admixed unless all ancestors are from same source population
-->

# Let the demonstration begin!

# Demo question #1

* Is the admixture timing of these two fluids:
  1. the same
  2. or different?

<!--
# Demo question #1 extension!

* Is the admixture timing of these two fluids:
  1. the same
  2. or different
  3. or it depends?
-->

# Demo question 2

* ~~Is the admixture timing of these two fluids the same or diff?~~
* Why is the mixing faster in one fluid compared to the other?
*

# Demo question 3

* ~~Is the admixture timing of these two fluids the same or diff?~~
* ~~Why is the mixing faster in one fluid compared to the other?~~
* Should I drink the red fluid or the green fluid?


# Microscale vs macroscale

* in the demo
  * molecules vs the glass
* in population genetics
  * recombination vs migrations
* in archaeology/history
  * mating vs migrations


# Current approaches are largely macroscale

* single pulse models (e.g. DATES [1])
  * assumes instantaneous gene flow
* multi-pulse & continuous immigration (e.g. MetHist [2])

&nbsp;

[1] Manjusha Chintalapati, Nick Patterson, Priya Moorjani (2022), The spatiotemporal patterns of major human admixture events during the European Holocene. doi:10.7554/eLife.77625

[2] Fortes-Lima, C.A., Laurent, R., Thouzeau, V., Toupance, B. and Verdu, P. (2021), Complex genetic admixture histories reconstructed with Approximate Bayesian Computation. doi:10.1111/1755-0998.13325

# Why I choose microscale

* lineal admixture time does not depend on statistical model
* the historical tendency of mixing populations
* distribution of lineal admixture time looks promising


# Outline

1. ~~Applications & Admixture ... with demo!!!~~
2. Lineages
3. An Estimator and Stochastic Process


# Lineages at a microscale

* per lineage in a genealogy (pedigree)
* per person in a population

![](genealogy2.jpg)


# Lineal admixture time

* time to first admixed individual in lineage

![](genealogy1.jpg)

* average across all lineages of all individuals in subpopulation

More details at castedo.com/doc/151


# Variations

* admixture generation number vs clock time
* how lineages are weighted
  * 50/50 parental split,
  + or actual genetic inheritance


# Outline

1. ~~Applications & Admixture ... with demo!!!~~
2. ~~Lineages~~
3. An Estimator and Stochastic Process


# Example estimator

Estimate the **average lineal admixture time** as
$$
\frac{1-\phi}{\phi} \left( 1 - \Sigma_i x_i^2 \right)
$$
where
$$
x_i = \frac{
   1 - \sqrt{1 - 4 \phi (1- \phi) \alpha_i}
 }{
   2(1-\phi)
}
$$
and $\alpha_i$ is the frequency of local ancestry (at loci)
from the $i$-th ancestral group.

![](genealogy1.jpg)


# What about $\phi$?

$$
\phi = 1 - \frac{\beta}{2 \alpha_0 (1-\alpha_0)}
$$

when

* there are only two ancestral source populations and
* $\beta$ is the frequency of diploid loci with dual ancestry.

&nbsp;

$\beta$ and $\alpha_0$ can be estimated from empirical data.


# Estimator derived from stochastic process

* stochastic process where lineages are underlying random object
* random variables of interest:
   * $M_t$ lineal admixture time
   * $A_t$ is ancestral source population of lineage

![](genealogy1.jpg)


# One-hot vector

Ancestral source of lineage, $A_t$,
represented by a one-hot vector; i.e. one of:

$$
\begin{aligned}
\mathrm{e}_0 & = (1, 0, 0, ...)  \\
\mathrm{e}_1 & = (0, 1, 0, ...)  \\
\mathrm{e}_2 & = (0, 0, 1, ...)  \\
...
\end{aligned}
$$

a.k.a. basis vector.

Term one-hot from electrical engineering.


# Mathematical Detail #1

Probability of lineal admixture time of zero.

$$
\newcommand{\e}{\mathrm{e}}
\mathrm{P}_{t+1}\{ M=0 \wedge A=\e_i \}
 = \phi \alpha_i + (1 - \phi)
 \mathrm{P}_{t}\{ M=0 \wedge A=\e_i \}^2
$$


# Mathematical Detail #2

$$
\begin{aligned}
\mathrm{E}_{t+1}[M]
  & = (\mathrm{E}_{t}[M | M>0] + 1) \mathrm{P}_t\{M>0\}^2 (1-\phi)  \\
  & + 2 \left(\frac{1}{2} \mathrm{E}_t[M | M>0] + 1\right) \mathrm{P}_t\{M>0\} \mathrm{P}_t\{M=0\} (1-\phi)  \\
  & + \left( \mathrm{P}_t\{M=0\}^2 - \sum_i \mathrm{P}_t\{M=0 \wedge A=e_i\}^2 \right) (1-\phi)
\end{aligned}
$$

<!--
# Stochastic Process of Gametic Lineages

Gametic lineage formally defined

castedo.com/doc/153
-->


# Assumptions of example stochastic process 

1. discrete time steps
2. infinite population
3. proportion $\alpha_i$ of immigrants from $i$-th ancestral group
4. fraction $\phi$ of population is new non-admixed immigrants
5. random mating (excluding new immigrants)
6. stationary process


# Consistent MLE Estimator (conjecture)

For the example stochastic process,

expected lineal admixture time = example estimator

More details at castedo.com/doc/154


# Further Work

* better stochastic process for better estimator
* validation with simulated data
* validation with data from known historical event
* software for empirical researchers


# Fin

Feedback & collaborations welcome!

Email castedo@castedo.com

